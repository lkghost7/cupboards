using System.Collections.Generic;
using Data;
using Items;
using Managers;
using UnityEngine;

namespace Levels
{
    public class LevelStart : MonoBehaviour
    {
        private const string NODE_NAME = "Node";
        private const string CHIP_NAME = "Chip";
        private const string LINK_NAME = "Link";

        private readonly List<Vector2Int> nodsPosition = new List<Vector2Int>();
        private readonly List<LevelData> levels = new List<LevelData>();
        private readonly Dictionary<int, DrawLinkData> linksDrawData = new Dictionary<int, DrawLinkData>();
        private readonly List<int> startPosChips = new List<int>();
        private readonly List<int> winPosChips = new List<int>();
        public readonly List<LinksPoint> linksPoint = new List<LinksPoint>();

        [SerializeField] private Chip chipPrefab;
        [SerializeField] private NodeCell nodePrefab;
        [SerializeField] private Link linkPrefab;
        public int ChipCount { get; private set; }

        private void Awake()
        {
            GetData();
        }

        public void StartLevel(int levelNum)
        {
            LoadDataLevels(levelNum);
            InitDrawLinks();
        }

        private void LoadDataLevels(int numLevel)
        {
            ChipCount = levels[numLevel].ChipCount;
            nodsPosition.AddRange(levels[numLevel].NodePosition);
            startPosChips.AddRange(levels[numLevel].StartPosChips);
            winPosChips.AddRange(levels[numLevel].WinPosChips);
            GetLinks(numLevel);
        }

        private void GetLinks(int numLevel)
        {
            foreach (var saveLinkPoint in levels[numLevel].SaveLinksPoints)
            {
                LinksPoint linkItem = new LinksPoint
                {
                    startLink = saveLinkPoint.startLink,
                    endLink = saveLinkPoint.endLink
                };
                linkItem.nodsPositionOne = nodsPosition[linkItem.startLink - 1];
                linkItem.nodsPositionTwo = nodsPosition[linkItem.endLink - 1];

                linksPoint.Add(linkItem);
            }
        }

        private void GetData()
        {
            levels.AddRange(DataLoader.LoadLevelData());
        }

        public void SpawnItem(Transform gridParent, Transform chipParent)
        {
            int count = 0;
            int countChip = 0;
            foreach (Vector2Int vector2Int in nodsPosition)
            {
                count++;
                NodeCell itemNode = Instantiate(nodePrefab, new Vector2(vector2Int.x, vector2Int.y), Quaternion.identity);
                itemNode.nodeNumber = count;
                var transformNode = itemNode.transform;
                transformNode.name = NODE_NAME + count;
                transformNode.parent = gridParent;

                if (startPosChips.Contains(count))
                {
                    countChip++;
                    Chip itemChip = Instantiate(chipPrefab, new Vector2(vector2Int.x, vector2Int.y), Quaternion.identity);
                    itemChip.SetNumChip(countChip);
                    itemChip.SetColorChip(ControlManager.Instance.GetColorPalette(countChip - 1)); 
                    var transformChip = itemChip.transform;
                    transformChip.name = CHIP_NAME + count;
                    transformChip.parent = chipParent;
                }
            }
        }

        private void InitDrawLinks()
        {
            int count = 0;

            foreach (LinksPoint point in linksPoint)
            {
                DrawLinkData drawLinkData = new DrawLinkData();

                drawLinkData.startLink = point.startLink;
                drawLinkData.vectorLink = point.GetVectorLink();
                linksDrawData.Add(count, drawLinkData);
                count++;
            }
        }

        public void DrawLinks(Transform parent)
        {
            int count = 0;

            foreach (var linkData in linksDrawData)
            {
                count++;

                int node = linkData.Value.startLink;
                Vector2Int directionNodeDraw = linkData.Value.vectorLink;
                Vector2Int positionLink = nodsPosition[node - 1];
                
                Link linkItem = Instantiate(linkPrefab, new Vector2(positionLink.x, positionLink.y), Quaternion.identity);
                linkItem.directionDraw(directionNodeDraw);
                var transformLink = linkItem.transform;
                transformLink.name = LINK_NAME + count;
                transformLink.parent = parent;
            }
        }

        public void InitWinCombination(List<int> winCombination)
        {
            winCombination.AddRange(winPosChips);
        }
    }
}