using Enum;
using Items;
using UnityEngine;

namespace Levels
{
    public struct LinksPoint
    {
        public int IdLink;
        public int startLink; 
        public int endLink; 
        public Vector2Int nodsPositionOne;
        public Vector2Int nodsPositionTwo;

        public Vector2Int GetVectorLink()
        {
            return nodsPositionOne.x == nodsPositionTwo.x ? Vector2Int.up : Vector2Int.right;
        }

        public DirectionNode GetNodeStartLinkStatus(NodeCell nodeCell) 
        {
            if (nodeCell.nodeNumber != startLink)
            {
                return DirectionNode.NONE;
            }
        
            if (nodsPositionOne.x == nodsPositionTwo.x)
            {
                nodeCell.isBlockUp = false;
                return DirectionNode.UP;
            }

            nodeCell.isBlockRight = false;
            return DirectionNode.RIGHT;
        }
    
        public DirectionNode GetNodeEndLinkStatus(NodeCell nodeCell) 
        {
            if (nodeCell.nodeNumber != endLink)
            {
                return DirectionNode.NONE;
            }
        
            if (nodsPositionOne.x == nodsPositionTwo.x)
            {
                nodeCell.isBlockDown = false;
                return DirectionNode.DOWN;
            }

            nodeCell.isBlockLeft = false;
            return DirectionNode.LEFT;
        }
    }
}
