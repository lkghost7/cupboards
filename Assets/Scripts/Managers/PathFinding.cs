using System;
using System.Collections.Generic;
using Enum;
using Items;
using Levels;
using UnityEngine;

namespace Managers
{
    public delegate NodeCell NodeDelegate(Vector2Int key);

    public class PathFinding : MonoBehaviour
    {
        private readonly Dictionary<Vector2Int, NodeCell> nodsDictionary = new Dictionary<Vector2Int, NodeCell>();
        private readonly Queue<NodeCell> queueNods = new Queue<NodeCell>();
        private readonly Vector2Int[] directions = {Vector2Int.up, Vector2Int.right, Vector2Int.down, Vector2Int.left};

        private DirectionNode directionNode;
        public Action<Chip> NodeTakenAction { get; private set; }
        public Action ClearNodeAction { get; private set; }
        public Action ResetSelectNodsAction { get; private set; }
        public NodeDelegate GetCurrentNode { get; private set; }

        public void InitPathFinding()
        {
            LoadGridNods();
            SetNodeStatusForPath();
            NodeTakenAction += NodeTaken;
            ClearNodeAction += ClearNods;
            ResetSelectNodsAction += RestSelectNods;
            GetCurrentNode += GetNode;
        }

        private NodeCell GetNode(Vector2Int key)
        {
            return nodsDictionary.ContainsKey(key) ? nodsDictionary[key] : null;
        }

        private void SetNodeStatusForPath()
        {
            LevelStart levelStart = ControlManager.Instance.GetLevelStart();
            List<LinksPoint> linkPoint = levelStart.linksPoint;

            foreach (LinksPoint linksPoint in linkPoint)
            {
                foreach (var nodeCell in nodsDictionary)
                {
                    NodeCell node = nodeCell.Value;
                    linksPoint.GetNodeStartLinkStatus(node);
                    linksPoint.GetNodeEndLinkStatus(node);
                }
            }
        }

        private void ClearNods()
        {
            foreach (KeyValuePair<Vector2Int, NodeCell> nods in nodsDictionary)
            {
                nods.Value.ClearCollider();
            }
        }

        private void RestSelectNods()
        {
            foreach (KeyValuePair<Vector2Int, NodeCell> nods in nodsDictionary)
            {
                nods.Value.ResetColor();
            }
        }

        private void NodeTaken(Chip chip)
        {
            NodeCell node = GetNode(chip.GetChipPosition());
            node.isEmpty = false;
        }

        public List<NodeCell> FindPath(NodeCell startNode, NodeCell endNode)
        {
            RefreshGrid();
            queueNods.Enqueue(startNode);
            startNode.isEmpty = true;
            while (queueNods.Count > 0)
            {
                NodeCell nodeCell = queueNods.Dequeue();

                if (nodeCell == endNode)
                {
                    break;
                }

                if (nodeCell.isEmpty)
                {
                    ExploredNeighbors(nodeCell);
                }

                nodeCell.isExplored = true;
            }

            List<NodeCell> path = new List<NodeCell>();

            if (endNode.exploredFrom == null)
            {
                return path;
            }

            NodeCell nextNode = endNode;

            path.Add(endNode);
            while (nextNode.exploredFrom != null)
            {
                path.Add(nextNode.exploredFrom);
                nextNode = nextNode.exploredFrom;
            }

            path.Reverse();
            return path;
        }

        private void RefreshNods()
        {
            foreach (NodeCell nodeCell in nodsDictionary.Values)
            {
                nodeCell.ResetNode();
                nodeCell.ResetColor();
                nodeCell.ClearCollider();
            }
        }

        private void RefreshGrid()
        {
            queueNods.Clear();
            foreach (NodeCell nodeCell in nodsDictionary.Values)
            {
                nodeCell.ResetNode();
            }
        }

        public void FindPointDestination(NodeCell nodeCell)
        {
            RefreshNods();

            nodeCell.FindFreePath(nodeCell);
            ControlManager.Instance.ClearNodsAction.Invoke();
        }

        public List<NodeCell> ExploredAllFreeNods(NodeCell nodeCell)
        {
            List<NodeCell> newGridList = new List<NodeCell>();

            directionNode = DirectionNode.NONE;
            foreach (Vector2Int direction in directions)
            {
                Vector2Int neighborCoordinates = nodeCell.GetNodePosition() + direction;

                directionNode++;

                switch (directionNode)
                {
                    case DirectionNode.UP:
                        if (nodeCell.isBlockUp)
                            continue;
                        break;
                    case DirectionNode.RIGHT:
                        if (nodeCell.isBlockRight)
                            continue;
                        break;
                    case DirectionNode.DOWN:
                        if (nodeCell.isBlockDown)
                            continue;
                        break;
                    case DirectionNode.LEFT:
                        if (nodeCell.isBlockLeft)
                            continue;
                        break;
                }

                if (nodsDictionary.ContainsKey(neighborCoordinates))
                {
                    NodeCell neighbour = nodsDictionary[neighborCoordinates];
                    if (neighbour.isEmpty) newGridList.Add(neighbour);
                }
            }

            return newGridList;
        }

        private void ExploredNeighbors(NodeCell nodeCell)
        {
            directionNode = DirectionNode.NONE;
            foreach (Vector2Int direction in directions)
            {
                directionNode++;

                switch (directionNode)
                {
                    case DirectionNode.UP:
                        if (nodeCell.isBlockUp)
                            continue;
                        break;
                    case DirectionNode.RIGHT:
                        if (nodeCell.isBlockRight)
                            continue;
                        break;
                    case DirectionNode.DOWN:
                        if (nodeCell.isBlockDown)
                            continue;
                        break;
                    case DirectionNode.LEFT:
                        if (nodeCell.isBlockLeft)
                            continue;
                        break;
                }

                Vector2Int neighborsCoordinates = nodeCell.GetNodePosition() + direction;

                if (nodsDictionary.ContainsKey(neighborsCoordinates))
                {
                    NodeCell neighbour = nodsDictionary[neighborsCoordinates];

                    if (queueNods.Contains(neighbour) || neighbour.isExplored)
                    {
                    }
                    else
                    {
                        queueNods.Enqueue(neighbour);
                        neighbour.exploredFrom = nodeCell;
                    }
                }
            }
        }

        private void LoadGridNods()
        {
            GridNods gridParent = ControlManager.Instance.GetGridParent();
            int count = gridParent.transform.childCount;
            for (int i = 0; i < count; i++)
            {
                NodeCell node = gridParent.transform.GetChild(i).GetComponent<NodeCell>();
                nodsDictionary.Add(node.GetNodePosition(), node);
            }
        }
    }
}