using System;
using System.Collections.Generic;
using interfaces;
using Items;
using Levels;
using Panels;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Managers
{
    public class ControlManager : GenericSingletonClass<ControlManager>
    {
        [Header("Parents for Spawn items")]
        [SerializeField] private GridNods gridParent;

        [SerializeField] private Transform linkParent;
        [SerializeField] private Chips chipsParent;
        
        [Header("Links")]
        [SerializeField] private LevelStart leveStart;
        [SerializeField] private PathFinding pathFinding;
        
        [Header("Panels")]
        [SerializeField] private StartPanel startPanel;
        [SerializeField] private WinPanel winPanel;

        [Header("Configure colors for node")]
        [SerializeField] private List<Color> colors;

        private readonly List<Chip> listChip = new List<Chip>();
        private NodeCell endPoint;
        private int winCount;
        public List<int> winCombination { get; private set; } = new List<int>();
        public Chip currentChip { get; set; }
        public bool ChipIsMove { get; set; }
        public Action<int> winStatusAction { get; private set; }
        public Action ClearSelectAction { get; private set; }
        public Action ClearNodsAction { get; private set; }

        private void Start()
        {
            startPanel.Show();
        }

        public void StartLevel(int numLevel)
        {
            leveStart.StartLevel(numLevel);
            leveStart.SpawnItem(gridParent.transform, chipsParent.transform);
            leveStart.InitWinCombination(winCombination);
            leveStart.DrawLinks(linkParent);

            InitChips();
            
            pathFinding.InitPathFinding();
            winStatusAction += ChekWins;
            ClearNodsAction += ClearNodsInChips;
            ClearSelectAction += ClearSelect;
        }
        
        public PathFinding GetPathFinding()
        {
            return pathFinding;
        }

        public LevelStart GetLevelStart()
        {
            return leveStart;
        }

        public GridNods GetGridParent()
        {
            return gridParent;
        }
        
        public Color GetColorPalette(int colorNum)
        {
            if (colors.Count >= colorNum) return colors[colorNum];
            int randomColor = Random.Range(0, colors.Count);
            return colors[randomColor];
        }
        
        private void ClearNodsInChips()
        {
            foreach (Chip chip in listChip)
            {
                pathFinding.GetCurrentNode?.Invoke(chip.GetChipPosition()).ClearCollider();
            }
        }
        
        private void ClearSelect()
        {
            foreach (Chip chip in listChip)
            {
                chip.Clear();
            }
        }

        private void ChekWins(int winChip)
        {
            winCount += winChip;

            if (winCount >= winCombination.Count)
            {
               winPanel.Show();
            }
        }

        private void InitChips()
        {
            var count = chipsParent.transform.childCount;
            for (var i = 0; i < count; i++)
            {
                Chip chip = chipsParent.transform.GetChild(i).GetComponent<Chip>();
                listChip.Add(chip);
            }
        }

        private void MoveToPointNode()
        {
            NodeCell chipPosition = pathFinding.GetCurrentNode?.Invoke(currentChip.GetChipPosition());
            currentChip.StartMove(chipPosition, endPoint);
        }

        private void Update()
        {
            if (!Input.GetKeyDown(KeyCode.Mouse0)) return;
            if (ChipIsMove)
                return;

            if (Camera.main is null) return;
            var MyRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (!Physics.Raycast(MyRay, out hit, 100)) return;
            ChekChip(hit);
            ChekNode(hit);
        }

        private void ChekChip(RaycastHit hit)
        {
            if (hit.collider.GetComponent<IMovableChip>() == null) return;
            ClearSelect();
            var choseChip = hit.collider.GetComponent<Chip>();
            choseChip.PrepareMove();
            currentChip.SelectChip();
        }

        private void ChekNode(RaycastHit hit)
        {
            if (hit.collider.GetComponent<ISelectableNode>() == null) return;
            var currentGreedCell = hit.collider.GetComponent<NodeCell>();
            endPoint = currentGreedCell;
            MoveToPointNode();
            ChipIsMove = true;
            pathFinding.ClearNodeAction?.Invoke();
        }
    }
}