using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Panels
{
    public class WinPanel : MonoBehaviour
    {
        [SerializeField] private Button restartButton;
        
        public void Show()
        {
            gameObject.SetActive(true);
            SetBtnRestart();
        }

        private void Hide()
        {
            gameObject.SetActive(false);
        }

        private void SetBtnRestart()
        {
            restartButton.onClick.AddListener(() =>
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            });
        }
    }
}