using System.Collections.Generic;
using Managers;
using UnityEngine;
using UnityEngine.UI;

namespace Panels
{
    public class StartPanel : MonoBehaviour
    {
        [SerializeField] private List<Button> buttonLevel;

        public void Show()
        {
            gameObject.SetActive(true);
            SetButton();
        }

        private void Hide()
        {
            gameObject.SetActive(false);
        }

        private void SetButton()
        {
            buttonLevel[0].onClick.AddListener(() =>
            {
                ControlManager.Instance.StartLevel(0);
                Hide();
            });

            buttonLevel[1].onClick.AddListener(() =>
            {
                ControlManager.Instance.StartLevel(1);
                Hide();
            });
        }
    }
}