using System.IO;
using UnityEngine;

namespace Data
{
    public static class DataLoader
    {
        private const string dataFolder = "LevelData";
        private const string txt = "*.txt";
        private static readonly string folderPath = $"{Application.dataPath}/{dataFolder}";

        private static LevelData LoadSave(string filePath)
        {
            if (File.Exists(filePath) == false)
            {
                return null;
            }

            LevelData levelData = new LevelData();

            return levelData.Deserialize(filePath);
        }

        public static LevelData[] LoadLevelData()
        {
            if (Directory.Exists(folderPath) == false)
            {
                Debug.LogError("no data in folder");
                return null;
            }

            var fileNames = Directory.GetFiles(folderPath, txt);
            LevelData[] data = new LevelData[fileNames.Length];
            for (int i = 0; i < data.Length; i++)
            {
                data[i] = LoadSave(fileNames[i]);
            }

            return data;
        }
    }
}