using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Data
{
    [Serializable]
    public class LevelData
    {
        private int id;
        private const int X = 0;
        private const int Y = 1;
        private const int START_LINK = 0;
        private const int END_LINK = 1;
        private const char SEPARATOR = ',';
        private const char SPACE = ' ';
        public int ChipCount { get; private set; }
        public int AvailablePoints { get; private set; }
        public Vector2Int[] NodePosition { get; private set; }
        public int[] StartPosChips { get; private set; }
        public int[] WinPosChips { get; private set; }
        public int LinksCount { get; private set; }
        public SaveLinksPoint[] SaveLinksPoints { get; private set; }

        public LevelData Deserialize(string filePath)
        {
            List<string> lines = new List<string>();
            using (StreamReader streamReader = new StreamReader(filePath, System.Text.Encoding.Default))
            {
                string line;
                while ((line = streamReader.ReadLine()) != null)
                {
                    lines.Add(line);
                }
            }

            var levelData = new LevelData();
            var step = 0;

            try
            {
                levelData.ChipCount = Convert.ToInt32(lines[step++]);
                levelData.AvailablePoints = Convert.ToInt32(lines[step++]);

                levelData.NodePosition = new Vector2Int[levelData.AvailablePoints];
                for (int i = 0; i < levelData.AvailablePoints; i++)
                {
                    lines[step].Trim(SPACE);
                    levelData.NodePosition[i].x = Convert.ToInt32(lines[step].Split(SEPARATOR)[X]);
                    levelData.NodePosition[i].y = Convert.ToInt32(lines[step++].Split(SEPARATOR)[Y]);
                }

                levelData.StartPosChips = new int[levelData.ChipCount];
                lines[step].Trim(SPACE);
                string[] temp = lines[step++].Split(SEPARATOR);
                for (int i = 0; i < temp.Length; i++)
                {
                    levelData.StartPosChips[i] = Convert.ToInt32(temp[i]);
                }

                levelData.WinPosChips = new int[levelData.ChipCount];
                lines[step].Trim(SPACE);
                temp = lines[step++].Split(SEPARATOR);
                for (int i = 0; i < temp.Length; i++)
                {
                    levelData.WinPosChips[i] = Convert.ToInt32(temp[i]);
                }

                levelData.LinksCount = Convert.ToInt32(lines[step++]);

                levelData.SaveLinksPoints = new SaveLinksPoint[levelData.LinksCount];
                for (int i = 0; i < levelData.LinksCount; i++)
                {
                    lines[step].Trim(SPACE);
                    levelData.SaveLinksPoints[i].startLink = Convert.ToInt32(lines[step].Split(SEPARATOR)[START_LINK]);
                    levelData.SaveLinksPoints[i].endLink = Convert.ToInt32(lines[step++].Split(SEPARATOR)[END_LINK]);
                }

                return levelData;
            }
            catch (Exception e)
            {
                Debug.Log($"Chek data file, error {e}");
                return null;
            }
        }
    }
}