
namespace Enum
{
    public enum ColorChips 
    {
        RED = 1,
        ORANGE = 2,
        BLACK = 3,
        BLUE = 4,
        PURPLE = 5,
        PINK = 6,
        YELLOW = 7,
        GRAY = 8,
        TURQUOISE = 9,
  
        RANDOM = 100
    }
}
