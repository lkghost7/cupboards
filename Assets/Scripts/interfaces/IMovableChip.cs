using Items;

namespace interfaces
{
    interface IMovableChip
    {
        void StartMove(NodeCell startNode, NodeCell endNode);
    }
}
