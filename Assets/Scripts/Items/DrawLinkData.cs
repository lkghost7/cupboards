using UnityEngine;

namespace Items
{
    public struct DrawLinkData
    {
        public int startLink;
        public Vector2Int vectorLink;
    }
}