using UnityEngine;

namespace Items
{
    public class Link : MonoBehaviour
    {
        public void directionDraw(Vector2Int direction)
        {
            if (direction != Vector2Int.up) transform.localRotation = Quaternion.Euler(0, 0, -90);
        }
    }
}