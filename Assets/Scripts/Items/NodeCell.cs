using System.Collections.Generic;
using System.Runtime.Serialization;
using Managers;
using UnityEngine;
using interfaces;

namespace Items
{
    [SelectionBase]
    public class NodeCell : MonoBehaviour, ISelectableNode
    {
        [Header("Color status fo node")]
        [SerializeField][Tooltip("Set passive node color")] private Color passiveNode = Color.gray;
        [SerializeField][Tooltip("Set active node color")] private Color activeNode = Color.green;
        
        private BoxCollider boxCollider;
        private SpriteRenderer spriteRenderer;
    
        public bool isExplored { get; set; }
        public bool isEmpty { get; set; }
        public bool isBlockUp { get; set; }
        public bool isBlockRight { get; set; }
        public bool isBlockDown { get; set; }
        public bool isBlockLeft { get; set; }
        public NodeCell exploredFrom { get; set; }
        
        private PathFinding pathFinding;
        public int nodeNumber { get; set; }

        private void Awake()
        {
            InitStatusNode();
        }

        private void Start()
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
            boxCollider = GetComponent<BoxCollider>();
            boxCollider.enabled = false;
            pathFinding = ControlManager.Instance.GetPathFinding();
        }

        private void InitStatusNode()
        {
            isEmpty = true;
            isBlockUp = true;
            isBlockRight = true;
            isBlockDown = true;
            isBlockLeft = true;
        }

        public Vector2Int GetNodePosition()
        {
            var position = transform.position;
            Vector2Int index = new Vector2Int((int) position.x, (int) position.y);
            return index;
        }
        public void ResetNode()
        {
            isExplored = false;
            exploredFrom = null;
        }

        public void ResetColor()
        {
            spriteRenderer.color = passiveNode;
        }

        public void ClearCollider()
        {
            boxCollider.enabled = false;
        }

        public void FindFreePath(NodeCell startCells)
        {
            if (!startCells.isExplored)
            {
                isExplored = true;
                spriteRenderer.color = activeNode;
                boxCollider.enabled = true;
                List<NodeCell> listCell = pathFinding.ExploredAllFreeNods(this);

                foreach (NodeCell gridCell in listCell)
                {
                    gridCell.FindFreePath(gridCell);
                }
            }
        }
    }
}