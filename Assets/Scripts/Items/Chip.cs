using System.Collections.Generic;
using Levels;
using Managers;
using UnityEngine;
using interfaces;

namespace Items
{
    public class Chip : MonoBehaviour, IMovableChip
    {
        [Header("Config prefab")]
        [SerializeField] private SpriteRenderer winStatus;

        [Header("Config chip")]
        [SerializeField][Tooltip("configure speed")] private float speed = 4;

        private List<NodeCell> waypoints;
        private PathFinding pathFinding;
        private NodeCell currentPosition;
        private NodeCell endPosition;
        private int currentIndex;
        private BoxCollider boxCollider;
        private SpriteRenderer spriteRenderer;
        private LevelStart levelStart;
        private Color chipColor;
        private int chipNumber;
        private bool isMove;
        private bool isSelect;

        void Start()
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
            pathFinding = ControlManager.Instance.GetPathFinding();
            levelStart = ControlManager.Instance.GetLevelStart();
            boxCollider = GetComponent<BoxCollider>();
            spriteRenderer.color = chipColor;
            pathFinding.NodeTakenAction?.Invoke(this);
        }

        public void SetColorChip(Color color)
        {
            chipColor = color;
        }

        public void SetNumChip(int num)
        {
            chipNumber = num;
        }

        public void SelectChip()
        {
            isSelect = true;
        }

        private void Update()
        {
            if (!isMove)
            {
                return;
            }

            Move();
        }

        private void ChekCurrentPositionNode()
        {
            NodeCell node = pathFinding.GetCurrentNode?.Invoke(GetChipPosition());
            List<int> winList = ControlManager.Instance.winCombination;

            if (winList.Count != levelStart.ChipCount)
            {
                Debug.LogError("Error config incorrect configuration amount chips");
                return;
            }

            var winComb = winList[chipNumber - 1];
            if (node is null || winComb != node.nodeNumber) return;
            node.isEmpty = false;
            winStatus.gameObject.SetActive(true);
            boxCollider.enabled = false;
            ControlManager.Instance.winStatusAction.Invoke(1);
        }

        public Vector2Int GetChipPosition()
        {
            var position = transform.position;
            return new Vector2Int((int) position.x, (int) position.y);
        }

        public void Clear()
        {
            spriteRenderer.color = chipColor;
            isSelect = false;
        }

        public void PrepareMove()
        {
            spriteRenderer.color = Color.white;
            NodeCell currentNode = pathFinding.GetCurrentNode?.Invoke(GetChipPosition());
            pathFinding.FindPointDestination(currentNode);
            ControlManager.Instance.currentChip = this;
        }

        public void StartMove(NodeCell startPos, NodeCell endPos)
        {
            waypoints = pathFinding.FindPath(startPos, endPos);
            endPosition = endPos;
            currentPosition = startPos;
            isMove = true;

            pathFinding.ResetSelectNodsAction?.Invoke();
            ControlManager.Instance.ClearSelectAction.Invoke();
        }

        private void Move()
        {
            if (currentPosition == endPosition && waypoints.Count == 0)
            {
                return;
            }

            var moveDelta = speed * Time.deltaTime;
            Vector2 targetPosition = waypoints[currentIndex].transform.position;
            transform.position = Vector2.MoveTowards(transform.position, targetPosition, moveDelta);
            if (!Mathf.Approximately(transform.position.x, targetPosition.x) ||
                !Mathf.Approximately(transform.position.y, targetPosition.y)) return;
            currentPosition = waypoints[currentIndex];

            if (currentPosition == endPosition)
            {
                currentIndex = 0;

                isMove = false;
                ControlManager.Instance.ChipIsMove = false;
                pathFinding.NodeTakenAction?.Invoke(this);
                ChekCurrentPositionNode();
            }
            else
            {
                currentIndex++;
            }
        }
    }
}